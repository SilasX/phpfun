<?php
class Car
{
  var $color;
  function Car($color="green")
  {
    $this->color = $color;
  }
  function what_color()
  {
    return $this->color;
  }
}
function myFunc() {
    static $y = 3;
    $zol = array(0, 1, true);
    while (true) {
        $y = $y + 1;
        return $y + $zol[1];
    }
}
$color="red";
$z = 3;
$y = 4;
$q = new Car("purple");
echo "y + z = " . myFunc(4) . "<br>";
echo "y + z = " . myFunc(4) . "<br>";
echo "My car is " . $color . "<br>";
echo "My car is " . $q->what_color() . "<br>";
?>
