<html>
<body>
<?php

function test_input($data) {
    return htmlspecialchars(stripslashes(trim($data)));
}

$name = $email = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = test_input($_POST["name"]);
    $email = test_input($_POST["email"]);
    $website = test_input($_POST["website"]);
    $comment = test_input($_POST["comment"]);
}

echo "Howdy, " . $name . " at email address " . $email;
?>
</body>
</html>
