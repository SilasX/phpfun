<?php
class Car
{
  var $color;
  function Car($color="green")
  {
    $this->color = $color;
  }
  function what_color()
  {
    return $this->color;
  }
}
function dayfunc($dateparam) {
    $date_hr = date($dateparam);
    if ($date_hr < 6) {
        return "early";
    } elseif ($date_hr < 18) {
        return "afternoon";
    } else {
        return "night";
    }
}
define("DONKEY", "I like donkeys");
echo "date value is " . dayfunc("H") . "<br>";
?>
