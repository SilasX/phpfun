<html>
<body>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
Name: <input type="text" name="name"><br>
E-mail: <input type="text" name="email"><br>
Website: <input type="text" name="website"><br>
Comment <textarea name="comment" rows="5" cols="40"></textarea>
<input type="submit">
</form>

<?php

function test_input($data) {
    return htmlspecialchars(stripslashes(trim($data)));
}

$name = $email = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = test_input($_POST["name"]);
    $email = test_input($_POST["email"]);
    $website = test_input($_POST["website"]);
    $comment = test_input($_POST["comment"]);
}

echo "Howdy, " . $name . " at email address " . $email;
?>

</body>
</html>
